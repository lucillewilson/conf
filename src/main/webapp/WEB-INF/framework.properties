APPLICATION_NAME=configuration
#	General Syntax:
#	The backslash \ is the line continuation character.
#		When the property supports multiple entries, the separator is the semi-colon ;
#
#	@since apbase 3.6 supports property_name+=value where value is concatenated to
#		to an existing value of that name. See Squish #17298 for more details
#	@since apwebapp 3.6 references to the SDK's html_element_definitions.txt and
#			DTR schema (transactionDefinitionBehavior.xsd) are no longer needed in
#			the application's properties file since they are loaded as resources
#			using the Java class loader.
#
#
# Example on how to override one of the product's lightbox jsps.
# Uncomment out the next line and the confirm_exit.jsp in the application context
# will be used instead of the SDK's
#lightbox_jsp_list=lightboxes/custom/confirm_exit.jsp

# Bootstrap configuration
# BOOT SERVICE PROVIDERS
# Specify the classes that implement the IBootServiceIntf
# interface.  These classes are classes that do some type
# of application boot strapping work and need to be loaded
# before the application can begin accepting requests.
# A semi-colon ; delimited list of classes
#boot_service_providers=com.agencyport.bootservice.ProductDefinitionsBootService
boot_service_providers=com.agencyport.configuration.bootservice.BootServiceProvider
# The following flag will force all XML product resources
# to undergo XSD validation at bootstrap time or any other time they are
# parsed afresh from the file system.
bootup_validate_schemas=true
# Link to other properties - LOB and otherwise
resources_root=${my_context_path}WEB-INF/
additional_properties_to_load=${resources_root}localization.properties;\
(asResource)properties/apdashboard.properties;\
${resources_root}cache.properties;\
${resources_root}version.properties
# REST API resource class registration.
application.rest_resource_classes=com.agencyport.configuration.security.filter.RequestFilter;\
com.agencyport.configuration.api.productdefinition.ProductDefinitionServiceResource;\
com.agencyport.configuration.api.searchindex.SolrIndexMappingResource;\
com.agencyport.configuration.api.worklistview.WorkListViewDefinitionResource;\
com.agencyport.configuration.api.workflow.WorkflowResource;\
com.agencyport.configuration.api.menu.MenuResource;\
com.agencyport.configuration.api.markdown.MarkdownResource;\
com.agencyport.configuration.api.groovy.GroovyResource;\
com.agencyport.configuration.api.endpoint.EndPointDiscoveryResource;\
com.agencyport.configuration.api.client.ClientResource;

# APDataCollection parameters
default_xml_pretty_format=true

# The following property instructs the SDK
# to use a different algorithm for id attribute
# values for those attribute values itself generates
# @see com.agencyport.domXML.APDataCollection#generateUID()
# @see com.agencyport.domXML.visitor.IdAttributeUpdater.getNextUID(APDataCollection)
# The following values are recognized:
#		1) docuid - uses an algorithm which significantly shortens the id attribute value
#				ensuring that the value is unique only within the scope of the XML document
#		2) guid (default if missing) - uses the traditional RandomGUID globally unique identifier
#			algorithm
system_generated_id_attribute_type=docuid

# Need this to generate modinfo's
should_retain_id_attributes=true

# Environment Config

my_context_path=${context_path}/

# DATABASE
#
# The JNDI name needs to be set up on your application server and
# match the datasource property value below.
# See your application server documentation to setup the JNDI connection.

#db_table_prefix
# will prefix tables in sql statements. This is used when the JNDI connection
# user id is different than the owner of the tables.  Value must have end with a dot ('.').
# Example:
#	db_table_prefix=AGENCYPORTAL.
db_table_prefix=

#datasource_prefix
# is used to prefix the JNDI connection name (datasource) with
# the required prefix.  Some application server require a prefix to be
# provided as part of the JNDI name.  Below some examples are provided
# for some of the application servers.
# examples:
#    Tomcat,Websphere ( if 'indirect' JNDI lookup type is used)
#     	datasource_prefix=java:comp/env/
#
#    Jboss
#       datasource_prefix=java:/
#
# this property can be left blank or the reference can be removed from the datasource property
datasource_prefix=java:comp/env/

#datasource
# is the name of the JNDI connection setup on your application server.
# this also includes a prefix if necessary.
# Note:  the datasource_prefix is not part of the JNDI name setup in your application server
datasource=${datasource_prefix}configuration

#database_agent_class_name
# The database_agent_class_name specifies the SDK database agent class to use for the application.
#		SQL Server support using sequences
#			database_agent_class_name=com.agencyport.database.SQLDatabaseAgent
#		SQL Server legacy 2008 support
#			database_agent_class_name=com.agencyport.database.LegacySQLDatabaseAgent
#
#		Oracle support
#			database_agent_class_name=com.agencyport.database.OracleDatabaseAgent
#
#		MySQL support
#			database_agent_class_name=com.agencyport.database.MySQLDatabaseAgent
#
#		DB2 support using CLOB
#			database_agent_class_name=com.agencyport.database.DB2DatabaseAgent
#		DB2 support using XML (recommended)
#			database_agent_class_name=com.agencyport.database.DB2XMLDatabaseAgent
database_agent_class_name=com.agencyport.database.MySQLDatabaseAgent

#JPA / Hibernate database settings
hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
hibernate.hbm2ddl.auto=none
hibernate.connection.charSet=UTF-8
hibernate.show_sql=false
hibernate.format_sql=true
hibernate.cache.region.factory_class=org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory
hibernate.cache.use_second_level_cache=true
hibernate.hibernate.globally_quoted_identifiers=true
hibernate.cache.use_query_cache=true




# Logging configuration
defer_logging_initialization=false

# Logging destination
# output_log_dir is used by the logging.properties file
output_log_dir=${catalina.base}/logs/${APPLICATION_NAME}

# application.logging_config_file contains logging configuration which
# is at application scope. Do not use JDK level logging configuration
# as it doesn't bode well in a shared JVM space with other applications
# that use JDK logging.
application.logging_config_file=${my_context_path}WEB-INF/application.logging.properties
logging_config_file_monitor=true
application.logger_name_prefix=${APPLICATION_NAME}

application.forward_writer_factory_classname=com.agencyport.rest.forwardwriter.ConfigSvcForwardWriterFactory

audit.event_database_manager_classname=com.agencyport.audit.core.NoOpAuditDBManager

security.profile_manager_classname=com.agencyport.security.profile.RedisSecurityProfileManager


#Distributed cache manager
distributed.cache.manager_classname=com.agencyport.cache.distributed.RedisDistributedCacheManager

# Client config yaml
client.config=classpath:config/batchloader/wayne/clientconfig.yaml;\
classpath:config/batchloader/prestige/clientconfig.yaml;\
classpath:config/batchloader/oscorp/clientconfig.yaml;\
classpath:config/batchloader/pei/clientconfig.yaml;\
classpath:config/batchloader/paragon/clientconfig.yaml;\
classpath:config/batchloader/sawyer/clientconfig.yaml;

batchload.config=classpath:config/batchloader/batchloader.yaml

# redis default port
redis.port=6379