/*
 * Created on Apr 14, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.rest.forwardwriter;

import java.io.IOException;
import java.io.Writer;

import javax.ws.rs.core.MediaType;

/**
 * The ConfigSvcForwardWriterFactory class
 */
public class ConfigSvcForwardWriterFactory extends ForwardWriterFactory {
    
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public IForwardWriter create(MediaType mediaType, Writer writer) throws IOException {
        if ( mediaType.getSubtype().equals(MediaType.APPLICATION_JSON_TYPE.getSubtype())){
            return new JSONForwardWriter(writer);
        } else {
            return new XMLForwardWriter(writer);
        }
    }

}
