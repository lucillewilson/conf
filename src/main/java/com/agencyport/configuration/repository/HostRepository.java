package com.agencyport.configuration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.agencyport.configuration.entity.jaxb.hosts.Host;

/**
 * 
 * The HostRepository class
 */
public interface HostRepository extends JpaRepository<Host, Long> {
	
	Host findById(int Id);
	Host findByGatewayHost(String gatewayHost);
	
	@Query(value = "SELECT client_id FROM host h where h.gateway_host = ?1", nativeQuery = true)
	Integer findClientIdByGatewayHost(String gatewayHost);
}