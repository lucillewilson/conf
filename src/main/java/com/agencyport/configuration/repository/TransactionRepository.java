/*
 * Created on Mar 23, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.agencyport.configuration.entity.jaxb.transdef.TTransaction;

/**
 * The ProductRepository class
 */
public interface TransactionRepository extends JpaRepository<TTransaction, Long> {

    /**
     * Find by product id.
     *
     * @param productId the product id
     * @return the list
     */
    @Query(value = "SELECT * FROM transaction t where t.product_auto_id = ?1", nativeQuery = true)
    List<TTransaction> findByProductId(int productId);

}
