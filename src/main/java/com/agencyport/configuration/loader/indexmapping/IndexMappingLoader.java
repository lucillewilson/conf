/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.indexmapping;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The IndexMappingLoader class
 */
@Service
public class IndexMappingLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
	
    final  Logger logger = LoggingManager.getLogger(IndexMappingLoader.class.getName());

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param indexMapping the index mapping
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.indexmapping.SearchIndex indexMapping, InputStream input) throws Exception {
			logger.info("Loading :"+indexMapping);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping.class);
			Unmarshaller u1 = jc1.createUnmarshaller();
			IndexMapping eIndexMapping = (IndexMapping) u1.unmarshal(input);
			eIndexMapping.setEffectiveDate(indexMapping.getEffective_date());
			try {
    			eIndexMapping.setCurrentVersion(indexMapping.isIs_current_version());
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			eIndexMapping.setClient(client);
    			client.getIndexMapping().add(eIndexMapping);
    			
    			eIndexMapping.getField().forEach(f->{
    				f.setIndexMapping(eIndexMapping);
    				f.getContent().forEach(co->{
    					co.setField(f);
    				});
    			});
    			em.persist(client);
    			em.flush();
    			em.getTransaction().commit();
    			em.close();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(indexMapping.toString(),exception);
			}

	}

	/**
	 * Read.
	 *
	 * @param indexMappingId the index mapping id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int indexMappingId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
			IndexMapping indexMapping = em.find(IndexMapping.class, indexMappingId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(indexMapping, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
