/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.view;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.view.TView;
import com.agencyport.configuration.entity.jaxb.view.TViews;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The ViewLoader class
 */
@Service
public class ViewLoader {
	
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(ViewLoader.class.getName());
    

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param view the view
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.view.View view, InputStream input) throws Exception {
        logger.info("Loading :"+view);
        EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
        EntityManager em = entityManagerFactory.createEntityManager();
        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.view.ObjectFactory.class);
        Unmarshaller u1 = jc1.createUnmarshaller();
        JAXBElement<TViews> jaxbTViews = (JAXBElement<TViews>) u1.unmarshal(input);
        TViews eTViews = jaxbTViews.getValue();
        try {
            for(TView eTView: eTViews.getView()) {
                em.getTransaction().begin();
                
                Object c = em.createQuery("select c from client c where c.name = :name")
                .setParameter("name", client.getName())
                .getResultList().stream().findFirst().orElse(null);
                
                if(c != null) {
                    client = (Client)c;
                }
                
                eTView.setClient(client);
                client.getView().add(eTView);
                eTView.setEffectiveDate(view.getEffective_date());
                eTView.setCurrentVersion(view.isIs_current_version());
                
                eTView.getCustomArgument().forEach(cArgs->{
                    cArgs.setView(eTView);
                    
                });
                eTView.getFieldSet().forEach(fs->{
                    fs.setView(eTView);
                    fs.getField().forEach(f->{
                        f.setFieldSet(fs);
                    });
                    fs.getDeleteField().forEach(dFs->{
                        dFs.setFieldSet(fs);
                    });
                });
                
                em.persist(client);
                em.flush();
                em.getTransaction().commit();
                
            }
        }catch(Exception exception) {
            try {
                if(em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
            }catch (Exception exception2) {
                ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
            }
            throw new Exception(view.toString(),exception);
        }finally{
            em.close();
        }

}
	
	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param view the view
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
    public void load( Client client,Product product,com.agencyport.configuration.loader.view.View view, InputStream input) throws Exception {
        logger.info("Loading :"+view);
        EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
        EntityManager em = entityManagerFactory.createEntityManager();
        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.view.ObjectFactory.class);
        Unmarshaller u1 = jc1.createUnmarshaller();
        JAXBElement<TViews> jaxbTViews = (JAXBElement<TViews>) u1.unmarshal(input);
        TViews eTViews = jaxbTViews.getValue();
        for(TView eTView: eTViews.getView()) {
            em.getTransaction().begin();
            
            Object c = em.createQuery("select c from client c where c.name = :name")
            .setParameter("name", client.getName())
            .getResultList().stream().findFirst().orElse(null);
            
            if(c != null) {
                client = (Client)c;
            }
            
            eTView.setClient(client);
            eTView.setProduct(product);
            product.getView().add(eTView);
            eTView.setEffectiveDate(product.getEffectiveDate());
            eTView.setCurrentVersion(product.isCurrrentVersion());
            
            eTView.getCustomArgument().forEach(cArgs->{
                cArgs.setView(eTView);
                
            });
            eTView.getFieldSet().forEach(fs->{
                fs.setView(eTView);
                fs.getField().forEach(f->{
                    f.setFieldSet(fs);
                });
                fs.getDeleteField().forEach(dFs->{
                    dFs.setFieldSet(fs);
                });
            });
            
            em.persist(eTView);
            em.flush();
            em.getTransaction().commit();
            
        }
        em.close();

}

	/**
	 * Read.
	 *
	 * @param viewId the view id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int viewId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.view.ObjectFactory.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    TView eTview = em.find(TView.class, viewId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(eTview, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
