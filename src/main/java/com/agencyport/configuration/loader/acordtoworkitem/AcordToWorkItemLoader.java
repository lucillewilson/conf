/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.acordtoworkitem;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The AcordToWorkItemLoader class
 */
@Service
public class AcordToWorkItemLoader {
	
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(AcordToWorkItemLoader.class.getName());

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param configAcordtoworkitemmap the config acordtoworkitemmap
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client, Product product, Acordtoworkitemmap configAcordtoworkitemmap, InputStream input) throws Exception {
	        logger.info("Loading :"+configAcordtoworkitemmap);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap acordtoworkitemmap = (com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap) u1.unmarshal(input);
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			Object  p =  em.createQuery("select p from product p where p.clientId = :clientId "
    					+ "and p.type = :type and p.version = :version and p.effectiveDate = :effectiveDate")
    					.setParameter("clientId", client.getId())
    					.setParameter("type", product.getType())
    					.setParameter("version", product.getVersion())
    					.setParameter("effectiveDate", product.getEffectiveDate())
    					.getResultList().stream().findFirst().orElse(null);
    			
    			if(p!=null) {
    				product = (Product)(p);
    			}
    			
    			product.setClient(client);
    			acordtoworkitemmap.setProduct(product);
    
    			acordtoworkitemmap.getMapEntry().forEach(mapEntry->{
    			    mapEntry.setAcordtoworkitemmap(acordtoworkitemmap);
    			});
    			em.persist(client);
    			em.persist(product);
    			em.persist(acordtoworkitemmap);
    			em.flush();
    			em.getTransaction().commit();
    			
			}catch (Exception exception) {
			    try {
			        if(em.getTransaction().isActive()) {
			            em.getTransaction().rollback();
			        }
			    }catch (Exception exception2) {
			        ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
			    }
			    throw new Exception(configAcordtoworkitemmap.toString(),exception);
    			
			}finally {
			    em.close();
			}

	}

	
	/**
	 * Read.
	 *
	 * @param acordToWorkItemMapId the acord to work item map id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int acordToWorkItemMapId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
		    com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap acordtoworkitemmap = em.find(com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap.class, acordToWorkItemMapId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(acordtoworkitemmap, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
