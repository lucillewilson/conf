/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.client;

import java.util.ArrayList;
import java.util.List;

import com.agencyport.configuration.loader.clientproperty.ClientProperty;
import com.agencyport.configuration.loader.dynamiclist.DynamicListTemplate;
import com.agencyport.configuration.loader.endpoint.EndPoint;
import com.agencyport.configuration.loader.groovyfiles.Groovy;
import com.agencyport.configuration.loader.hosts.Hosts;
import com.agencyport.configuration.loader.indexmapping.SearchIndex;
import com.agencyport.configuration.loader.industrycode.IndustryCodeList;
import com.agencyport.configuration.loader.markdown.Markdown;
import com.agencyport.configuration.loader.menu.Menu;
import com.agencyport.configuration.loader.optionlist.OptionList;
import com.agencyport.configuration.loader.product.Product;
import com.agencyport.configuration.loader.transformer.Transformer;
import com.agencyport.configuration.loader.view.View;
import com.agencyport.configuration.loader.wcclasscode.WorkCompClassCodeList;
import com.agencyport.configuration.loader.workflow.WorkFlow;
import com.agencyport.configuration.loader.workitemassistant.WorkItemAssistant;
import com.agencyport.configuration.loader.worklistview.WorkListView;

/**
 * The Client class
 */
public final class Client {
    
    /**
     * The <code>name</code>.
     */
    private int id;

	/**
	 * The <code>name</code>.
	 */
	private String name;
	
	/**
	 * The <code>description</code>.
	 */
	private String description;
	
	/**
	 * The <code>product</code>.
	 */
	private List<Product> product;
	
	/**
	 * The <code>searchindex</code>.
	 */
	private List<SearchIndex> searchindex;
	
	/**
	 * The <code>worklistview</code>.
	 */
	private List<WorkListView> worklistview;
	
	/**
	 * The <code>workflow</code>.
	 */
	private List<WorkFlow> workflow;
	
	/**
	 * The <code>optionlist</code>.
	 */
	private List<OptionList> optionlist;
	
	/**
	 * The <code>dynamiclisttemplate</code>.
	 */
	private List<DynamicListTemplate> dynamiclisttemplate;
	
	/**
	 * The <code>view</code>.
	 */
	private List<View> view;
	
	/**
	 * The <code>industrycodelist</code>.
	 */
	private List<IndustryCodeList> industrycodelist;
	
	/**
	 * The <code>workcompclasscodelist</code>.
	 */
	private List<WorkCompClassCodeList> workcompclasscodelist;
	
	/**
     * The <code>menu</code>.
     */
    private List<Menu> menu;
    
    /**
     * The <code>transformer</code>.
     */
    private List<Transformer> transformer;
    
    /**
     * The <code>workitemassistant</code>.
     */
    private List<WorkItemAssistant> workitemassistant;
	
    /**
     * The <code>markdown</code> for custom static pages.
     */
    private List<Markdown> markdown;
    
    /**
     * The <code>groovy</code> files
     */
    private List<Groovy> groovy;
    
    /**
     * The <code>endPoint</code>.
     */
    private List<EndPoint> endpoint;
    
    /**
     * The <code>endPoint</code>.
     */
    private List<ClientProperty> clientProperty;
    
    /**
     * The <code>endPoint</code>.
     */
    private List<Hosts> clientHosts;
    
	/**
     * Returns the id	
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id to id
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the product.
	 *
	 * @return the product
	 */
	public List<Product> getProduct() {
		if(product== null) {
			product = new ArrayList<Product>();
		}
		return product;
	}

	/**
	 * Sets the product.
	 *
	 * @param product the new product
	 */
	public void setProduct(List<Product> product) {
		this.product = product;
	}

	/**
	 * Gets the searchindex.
	 *
	 * @return the searchindex
	 */
	public List<SearchIndex> getSearchindex() {
		if(searchindex==null) {
			searchindex = new ArrayList<SearchIndex>();
		}
		return searchindex;
	}

	/**
	 * Sets the searchindex.
	 *
	 * @param searchindex the new searchindex
	 */
	public void setSearchindex(List<SearchIndex> searchindex) {
		this.searchindex = searchindex;
	}
	
	/**
	 * Gets the worklistview.
	 *
	 * @return the worklistview
	 */
	public List<WorkListView> getWorklistview() {
		
		if(worklistview==null) {
			worklistview = new ArrayList<WorkListView>();
		}
		return worklistview;
	}
	
	/**
	 * Sets the worklistview.
	 *
	 * @param worklistview the new worklistview
	 */
	public void setWorklistview(List<WorkListView> worklistview) {
		this.worklistview = worklistview;
	}

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("name: "+name);
		buffer.append(" description: "+description);
		return buffer.toString();
	}

    /**
     * Gets the workflow.
     *
     * @return the workflow
     */
    public List<WorkFlow> getWorkflow() {
        if(workflow==null) {
            workflow = new ArrayList<WorkFlow>();
        }
        return workflow;
    }

    /**
     * Sets the workflow.
     *
     * @param workflow the new workflow
     */
    public void setWorkflow(List<WorkFlow> workflow) {
        this.workflow = workflow;
    }

    /**
     * Gets the optionlist.
     *
     * @return the optionlist
     */
    public List<OptionList> getOptionlist() {
        if(optionlist==null) {
            optionlist = new ArrayList<OptionList>();
        }
        return optionlist;
    }

    /**
     * Sets the optionlist.
     *
     * @param optionlist the new optionlist
     */
    public void setOptionlist(List<OptionList> optionlist) {
        this.optionlist = optionlist;
    }

    /**
     * Gets the dynamiclisttemplate.
     *
     * @return the dynamiclisttemplate
     */
    public List<DynamicListTemplate> getDynamiclisttemplate() {
        if(dynamiclisttemplate==null) {
            dynamiclisttemplate = new ArrayList<DynamicListTemplate>();
        }
        return dynamiclisttemplate;
    }

    /**
     * Sets the dynamiclisttemplate.
     *
     * @param dynamiclisttemplate the new dynamiclisttemplate
     */
    public void setDynamiclisttemplate(List<DynamicListTemplate> dynamiclisttemplate) {
        this.dynamiclisttemplate = dynamiclisttemplate;
    }

    /**
     * Gets the view.
     *
     * @return the view
     */
    public List<View> getView() {
        if(view==null) {
            view = new ArrayList<View>();
        }
        return view;
    }

    /**
     * Sets the view.
     *
     * @param view the new view
     */
    public void setView(List<View> view) {
        this.view = view;
    }

    /**
     * Gets the industrycodelist.
     *
     * @return the industrycodelist
     */
    public List<IndustryCodeList> getIndustrycodelist() {
        if(industrycodelist==null){
            industrycodelist = new ArrayList<IndustryCodeList>();
        }
        return industrycodelist;
    }

    /**
     * Sets the industrycodelist.
     *
     * @param industrycodelist the new industrycodelist
     */
    public void setIndustrycodelist(List<IndustryCodeList> industrycodelist) {
        this.industrycodelist = industrycodelist;
    }

    /**
     * Gets the workcompclasscodelist.
     *
     * @return the workcompclasscodelist
     */
    public List<WorkCompClassCodeList> getWorkcompclasscodelist() {
        if(workcompclasscodelist==null){
            workcompclasscodelist = new ArrayList<WorkCompClassCodeList>();
        }
        return workcompclasscodelist;
    }

    /**
     * Sets the workcompclasscodelist.
     *
     * @param workcompclasscodelist the new workcompclasscodelist
     */
    public void setWorkcompclasscodelist(List<WorkCompClassCodeList> workcompclasscodelist) {
        this.workcompclasscodelist = workcompclasscodelist;
    }

    /**
     * Returns the menu	
     * @return the menu
     */
    public List<Menu> getMenu() {
        if(menu==null){
            menu = new ArrayList<Menu>();
        }
        return menu;
    }

    /**
     * Returns the transformer	
     * @return the transformer
     */
    public List<Transformer> getTransformer() {
        if(transformer==null){
            transformer = new ArrayList<Transformer>();
        }
        return transformer;
    }

    /**
     * Returns the workitemassistant	
     * @return the workitemassistant
     */
    public List<WorkItemAssistant> getWorkitemassistant() {
        if(workitemassistant==null){
            workitemassistant = new ArrayList<WorkItemAssistant>();
        }
        return workitemassistant;
    }
    
    /**
     * Returns the markdown    
     * @return the markdown
     */
    public List<Markdown> getMarkdown() {
        if(markdown==null){
            markdown = new ArrayList<Markdown>();
        }
        return markdown;
    }

    /**
     * Sets the menu to menu
     * @param menu the menu to set
     */
    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

    /**
     * Sets the transformer to transformer
     * @param transformer the transformer to set
     */
    public void setTransformer(List<Transformer> transformer) {
        this.transformer = transformer;
    }

    /**
     * Sets the workitemassistant to workitemassistant
     * @param workitemassistant the workitemassistant to set
     */
    public void setWorkitemassistant(List<WorkItemAssistant> workitemassistant) {
        this.workitemassistant = workitemassistant;
    }


    /**
     * Sets the markdown to markdown
     * @param markdown the markdown to set
     */
    public void setMarkdown(List<Markdown> markdown) {
        this.markdown = markdown;
    }

    /**
     * Returns the groovy list  
     * @return the groovy list
     */
	public List<Groovy> getGroovy() {
		if(groovy == null){
			groovy = new ArrayList<Groovy>();
		}
		return groovy;
	}

    /**
     * Sets the groovy list to groovy
     * @param groovy the groovy list to set
     */
	public void setGroovy(List<Groovy> groovy) {
		this.groovy = groovy;
	}		
    /**
     * Returns the endpoints list  
     * @return the endpoints list
     */
	public List<EndPoint> getEndpoint() {
		if(endpoint == null){
			endpoint = new ArrayList<EndPoint>();
		}
		return endpoint;
	}
   /**
     * Sets the endpoints list to endpoint
     * 
     * @param endpoint the endpoint list to set
     */
	public void setEndpoint(List<EndPoint> endpoint){
		this.endpoint = endpoint;
	}

	public List<ClientProperty> getClientProperty() {
		return clientProperty;
	}

	public void setClientProperty(List<ClientProperty> clientProperty) {
		this.clientProperty = clientProperty;
	}

	public List<Hosts> getClientHosts() {
		return clientHosts;
	}

	public void setClientHosts(List<Hosts> clientHosts) {
		this.clientHosts = clientHosts;
	}


}
