/*
 * Created on Mar 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.client;

import java.util.Date;
import java.util.List;

/**
 * The Class ClientConfig.
 */
public final class ClientConfig {
	
	/**
	 * The <code>loaddate</code>.
	 */
	private Date loaddate;
	
	/**
	 * The <code>client</code>.
	 */
	private List<Client> client;
	
	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	public List<Client> getClient() {
		return client;
	}

	/**
	 * Sets the client.
	 *
	 * @param client the new client
	 */
	public void setClient(List<Client> client) {
		this.client = client;
	}

	/**
	 * Gets the loaddate.
	 *
	 * @return the loaddate
	 */
	public Date getLoaddate() {
		return loaddate;
	}

	/**
	 * Sets the loaddate.
	 *
	 * @param loaddate the new loaddate
	 */
	public void setLoaddate(Date loaddate) {
		this.loaddate = loaddate;
	}

	
	
	

}
