package com.agencyport.configuration.loader.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.yaml.snakeyaml.Yaml;

import com.agencyport.utils.AppProperties;

public class ClientConfigHolder {

    private List<ClientConfig> clientConfig;

    public ClientConfigHolder(ApplicationContext context){
        clientConfig = new ArrayList<ClientConfig>();
        List<String> configLocations = AppProperties.getAppProperties().getPropertyAsTokens("client.config");
        for(String configLocation: configLocations){
            Resource res =  context.getResource(configLocation);
            InputStream stream = null;
            try {
                stream =  res.getInputStream();
                ClientConfig config = new Yaml().loadAs(stream, ClientConfig.class);
                clientConfig.add(config);
            } catch (IOException e) {
                // TODO Error Handling
            }
        }
        
    }
    
    public List<ClientConfig> getClientConfig() {
        return clientConfig;
    }

    public void setClientConfig(List<ClientConfig> clientConfig) {
        this.clientConfig = clientConfig;
    }
    
    
    
}

