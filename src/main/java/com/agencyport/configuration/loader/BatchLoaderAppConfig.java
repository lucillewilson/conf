/*
 * Created on Mar 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader;

import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.agencyport.configuration.loader.acordtoworkitem.AcordToWorkItemLoader;
import com.agencyport.configuration.loader.behavior.TDBehaviorLoader;
import com.agencyport.configuration.loader.clientproperty.ClientPropertyLoader;
import com.agencyport.configuration.loader.dynamiclist.DynamicTemplateListLoader;
import com.agencyport.configuration.loader.endpoint.EndPointsLoader;
import com.agencyport.configuration.loader.groovyfiles.GroovyLoader;
import com.agencyport.configuration.loader.hosts.HostsLoader;
import com.agencyport.configuration.loader.indexmapping.IndexMappingLoader;
import com.agencyport.configuration.loader.industrycode.IndustryCodeLoader;
import com.agencyport.configuration.loader.markdown.MarkdownLoader;
import com.agencyport.configuration.loader.menu.MenuLoader;
import com.agencyport.configuration.loader.optionlist.OptionListLoader;
import com.agencyport.configuration.loader.pdf.PdfdefinitionsLoader;
import com.agencyport.configuration.loader.transdef.TDLoader;
import com.agencyport.configuration.loader.transformer.TransformerLoader;
import com.agencyport.configuration.loader.view.ViewLoader;
import com.agencyport.configuration.loader.wcclasscode.WorkCompClassCodeLoader;
import com.agencyport.configuration.loader.workflow.WorkFlowLoader;
import com.agencyport.configuration.loader.workitemassistant.WorkItemAssistantLoader;
import com.agencyport.configuration.loader.worklistview.WorkListViewLoader;
import com.agencyport.configuration.loader.xarcrules.XarcRulesLoader;
import com.agencyport.logging.LoggingManager;

import liquibase.integration.spring.SpringLiquibase;

/**
 * 
 * The Spring LoadAppConfig class
 */
@Configuration
@EnableJpaRepositories(basePackages = { "com.agencyport.configuration.repository" })
public class BatchLoaderAppConfig {
    
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(BatchLoaderAppConfig.class.getName());
    /**
     * The <code>environment</code>.
     */
    @Autowired
    private Environment environment;
    
    /**
     * Gets the test data source.
     *
     * @return the test data source
     */
    @Bean(name="dataSource", destroyMethod="")
    public DataSource getTestDataSource() {
        String jndiDatasourceName = environment.getProperty("datasource", String.class,null);
        if("null".equals(jndiDatasourceName)) {
            jndiDatasourceName = null;
        }
        if(jndiDatasourceName == null) {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setUsername(environment.getProperty("db.jdbc_username"));
            dataSource.setPassword(environment.getProperty("db.jdbc_password"));
            dataSource.setDriverClassName(environment.getProperty("db.jdbc_driver"));
            dataSource.setUrl(environment.getProperty("db.database_url"));
            return  (DataSource) dataSource;
        }
        else {
            JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
            dataSource.setJndiName(jndiDatasourceName);
            try {
                dataSource.afterPropertiesSet();
            } catch (IllegalArgumentException | NamingException e) {
                throw new RuntimeException(e);
            }
            return (DataSource) dataSource.getObject();
        }
    
    }
    @Bean(name = "liquibase")
    public SpringLiquibase liquibase(DataSource dataSource) {
        logger.info("Initializing liquibase runner against datasource");
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:config/liquibase/master.xml");
        return liquibase;
    }
		
	/**
	 * Transaction manager.
	 *
	 * @param entityManagerFactory the entity manager factory
	 * @return the jpa transaction manager
	 */
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
	
	/**
	 * Entity manager factory.
	 *
	 * @param dataSource the data source
	 * @return the local container entity manager factory bean
	 */
	@Bean
	@DependsOn("liquibase")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
		
		logger.info("Configuring LocalContainerEntityManagerFactoryBean");
				
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.agencyport.configuration.entity");
        
        Properties jpaProperties = new Properties();

		// Configures the used database dialect. This allows Hibernate to create
		// SQL
		// that is optimized for the used database.
		jpaProperties.put("hibernate.dialect", environment.getProperty("hibernate.dialect", org.hibernate.dialect.MySQL5InnoDBDialect.class.getName()));

		// Specifies the action that is invoked to the database when the
		// Hibernate
		// SessionFactory is created or closed.
		jpaProperties.put("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto", "none"));

		// If the value of this property is true, Hibernate writes all SQL
		// statements to the console.
		jpaProperties.put("hibernate.show_sql", environment.getProperty("hibernate.show_sql", Boolean.class, true));

		// If the value of this property is true, Hibernate will format the SQL
		// that is written to the console.
		jpaProperties.put("hibernate.format_sql", environment.getProperty("hibernate.format_sql", Boolean.class, true));

		jpaProperties.put("hibernate.connection.charSet", environment.getProperty("hibernate.connection.charSet", "UTF-8"));
		jpaProperties.put("hibernate.cache.use_second_level_cache", environment.getProperty("hibernate.cache.use_second_level_cache", Boolean.class, false));
		
		jpaProperties.put("hibernate.globally_quoted_identifiers",environment.getProperty("hibernate.globally_quoted_identifiers", Boolean.class, true));

		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;
    }

	/**
	 * Creates the td loader.
	 *
	 * @return the TD loader
	 */
	@Bean
	public TDLoader createTDLoader() {
	    return new TDLoader();
	}
	
	/**
	 * Creates the td behavior loader.
	 *
	 * @return the TD behavior loader
	 */
	@Bean
    public TDBehaviorLoader createTDBehaviorLoader() {
        return new TDBehaviorLoader();
    }
	
	/**
	 * Creates the index mapping loader.
	 *
	 * @return the index mapping loader
	 */
	@Bean
    public IndexMappingLoader createIndexMappingLoader() {
        return new IndexMappingLoader();
    }
	
	/**
	 * Creates the groovy loader.
	 *
	 * @return the groovy loader
	 */
	@Bean
    public GroovyLoader createGroovyLoader() {
        return new GroovyLoader();
    }
	
	/**
	 * Creates the markdown loader.
	 *
	 * @return the markdown loader
	 */
	@Bean
    public MarkdownLoader createMarkdownLoader() {
        return new MarkdownLoader();
    }
	
	/**
	 * Creates the work list view loader.
	 *
	 * @return the work list view loader
	 */
	@Bean
    public WorkListViewLoader createWorkListViewLoader() {
        return new WorkListViewLoader();
    }
	
	/**
	 * Creates the view loader.
	 *
	 * @return the view loader
	 */
	@Bean
    public ViewLoader createViewLoader() {
        return new ViewLoader();
    }
	
	/**
	 * Creates the acord to work item loader.
	 *
	 * @return the acord to work item loader
	 */
	@Bean
    public AcordToWorkItemLoader createAcordToWorkItemLoader() {
        return new AcordToWorkItemLoader();
    }
	
	/**
	 * Creates the work flow loader.
	 *
	 * @return the work flow loader
	 */
	@Bean
    public WorkFlowLoader createWorkFlowLoader() {
        return new WorkFlowLoader();
    }
	
	/**
	 * Creates the option list loader.
	 *
	 * @return the option list loader
	 */
	@Bean
    public OptionListLoader createOptionListLoader() {
        return new OptionListLoader();
    }
	
	/**
	 * Creates the dynamic template list loader.
	 *
	 * @return the dynamic template list loader
	 */
	@Bean
    public DynamicTemplateListLoader createDynamicTemplateListLoader() {
        return new DynamicTemplateListLoader();
    }
	
	/**
	 * Creates the work comp class code loader.
	 *
	 * @return the work comp class code loader
	 */
	@Bean
    public WorkCompClassCodeLoader createWorkCompClassCodeLoader() {
        return new WorkCompClassCodeLoader();
    }
	
	/**
	 * Creates the industry code loader.
	 *
	 * @return the industry code loader
	 */
	@Bean
    public IndustryCodeLoader createIndustryCodeLoader() {
        return new IndustryCodeLoader();
    }
	
	/**
	 * Creates the xarc rules loader.
	 *
	 * @return the xarc rules loader
	 */
	@Bean
    public XarcRulesLoader createXarcRulesLoader() {
        return new XarcRulesLoader();
    }
	
	/**
	 * Creates the menu loader.
	 *
	 * @return the menu loader
	 */
	@Bean
    public MenuLoader createMenuLoader() {
        return new MenuLoader();
    }
	
	/**
	 * Creates the transformer loader.
	 *
	 * @return the transformer loader
	 */
	@Bean
    public TransformerLoader createTransformerLoader() {
        return new TransformerLoader();
    }
	
	/**
	 * Creates the work item assistant loader.
	 *
	 * @return the work item assistant loader
	 */
	@Bean
    public WorkItemAssistantLoader createWorkItemAssistantLoader() {
        return new WorkItemAssistantLoader();
    }
	
	/**
	 * Creates the batch loader.
	 *
	 * @return the batch loader
	 */
	@Bean
    public BatchLoader createBatchLoader() {
        return new BatchLoader();
    }
	
	/**
     * Creates the PDFDefinition loader.
     *
     * @return the PDFDefinition Loader
     */
    @Bean
    public PdfdefinitionsLoader createPDFDefinitionLoader() {
        return new PdfdefinitionsLoader();
    }
    /**
     * Creates the EndPoints loader.
     *
     * @return the EndPoints Loader
     */
    @Bean
    public EndPointsLoader createEndPointsLoader() {
        return new EndPointsLoader();
    }
	
    /**
     * Creates the clientProperty loader.
     *
     * @return the clientProperty Loader
     */
    @Bean
    public ClientPropertyLoader createClientPropertyLoader() {
        return new ClientPropertyLoader();
    }
    
    /**
     * Creates the clientProperty loader.
     *
     * @return the clientProperty Loader
     */
    @Bean
    public HostsLoader createHostsLoader() {
        return new HostsLoader();
    }
}
