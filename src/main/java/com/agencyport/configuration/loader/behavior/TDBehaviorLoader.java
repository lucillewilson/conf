/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.behavior;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The TDBehaviorLoader class
 */
@Service
public class TDBehaviorLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(TDBehaviorLoader.class.getName());

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param configBehavior the config behavior
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client, Product product, Behavior configBehavior , InputStream input) throws Exception {
			logger.info("Loading :"+configBehavior);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			TransactionDefinitionBehavior transactionDefinitionBehavior = (TransactionDefinitionBehavior) u1.unmarshal(input);
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			Object  p =  em.createQuery("select p from product p where p.clientId = :clientId "
    					+ "and p.type = :type and p.version = :version and p.effectiveDate = :effectiveDate")
    					.setParameter("clientId", client.getId())
    					.setParameter("type", product.getType())
    					.setParameter("version", product.getVersion())
    					.setParameter("effectiveDate", product.getEffectiveDate())
    					.getResultList().stream().findFirst().orElse(null);
    			
    			if(p!=null) {
    				product = (Product)(p);
    			}
    			
    			product.setClient(client);
    			transactionDefinitionBehavior.setProduct(product);
    			product.gettTransactions().stream().filter(tran -> {
    				return tran.getId().equals(configBehavior.getTransaction());
    				
    			}).forEach(tran -> {
    				tran.getTransactionDefinitionBehavior().add(transactionDefinitionBehavior);
    				transactionDefinitionBehavior.getTransaction().add(tran);
    			});
    			if(transactionDefinitionBehavior.getWhere() != null) {
    				transactionDefinitionBehavior.getWhere().setTransactionDefinitionBehavior(transactionDefinitionBehavior);
    				
    				transactionDefinitionBehavior.getWhere().getPreCondition().forEach(_precondition->{
    					_precondition.setWhere(transactionDefinitionBehavior.getWhere());
    				});
    			}
    			
    			transactionDefinitionBehavior.getAdditionalFieldToShred().forEach(_addF -> {
    				_addF.setTransactionDefinitionBehavior(transactionDefinitionBehavior);
    			});
    			transactionDefinitionBehavior.getHotField().forEach(_hot -> {
    				_hot.setTransactionDefinitionBehavior(transactionDefinitionBehavior);
    				
    				_hot.getRefreshListFieldEvent().forEach(_refreshListFieldEvent -> {
    					_refreshListFieldEvent.settHotFieldType(_hot);
    				});
    				
    			});
    			transactionDefinitionBehavior.getBehavior().forEach(_behavior-> {
    				_behavior.setTransactionDefinitionBehavior(transactionDefinitionBehavior);
    				_behavior.transferToEntity();
    				
    				
    			});
    			
    			em.persist(client);
    			em.persist(product);
    			em.persist(transactionDefinitionBehavior);
    			em.flush();
    			em.getTransaction().commit();
			}catch (Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(configBehavior.toString(),exception);
			}finally {
			    em.close();
			}

	}

	/**
	 * Read.
	 *
	 * @param transactionDefinitionBehaviorId the transaction definition behavior id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int transactionDefinitionBehaviorId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
			TransactionDefinitionBehavior transactionDefinitionBehavior = em.find(TransactionDefinitionBehavior.class, transactionDefinitionBehaviorId);
			
			if(transactionDefinitionBehavior.getWhere() != null) {
				transactionDefinitionBehavior.getWhere().getPreCondition().forEach(_x ->{});
			}
			transactionDefinitionBehavior.getAdditionalFieldToShred().forEach(x->{});
			transactionDefinitionBehavior.getHotField().forEach(_x->{
				_x.getRefreshListFieldEvent();
			});
			transactionDefinitionBehavior.getBehavior().forEach(_b ->{
				_b.transferFromEntity();
			});
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(transactionDefinitionBehavior, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
