/*
 * Created on Jan 26, 2017 by btran AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.markdown;

import java.io.InputStream;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.markdown.MarkdownPojo;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The MarkdownLoader class, use to load markdown files into database
 */
@Service
public class MarkdownLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
	
    final  Logger logger = LoggingManager.getLogger(MarkdownLoader.class.getName());


	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param markdown the markdown loader object
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Product product, Client client, Markdown markdown, InputStream input) throws Exception {
			logger.info("Loading :"+ markdown);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
				//constructs the blob from inputstream
                StringBuilder builder = new StringBuilder();
                int ch;
                while((ch = input.read()) != -1){
                    builder.append((char)ch);
                }
                String blob = builder.toString();
                
                MarkdownPojo markdownObj = new MarkdownPojo();
                markdownObj.setProduct(product);
                markdownObj.setmarkdownName(markdown.getName());
                markdownObj.setmarkdownFile(blob);
                markdownObj.setClient(client);
                
                //persists the blob into database
    			em.getTransaction().begin();
    			em.persist(markdownObj);
    			em.flush();
    			em.getTransaction().commit();
			}catch (Exception exception){
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception("Error loading markdown file: " + markdown.getName() + ": Markdown loader stopping. ", exception);
			}finally {
			    em.close();
			}
	}
}
