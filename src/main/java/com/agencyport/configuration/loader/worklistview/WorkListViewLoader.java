/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.worklistview;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The WorkListViewLoader class
 */
@Service
public class WorkListViewLoader {
	
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(WorkListViewLoader.class.getName());
    

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param workListView the work list view
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.worklistview.WorkListView workListView, InputStream input) throws Exception {
			logger.info("Loading :"+workListView);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			WorkListViewDefinition eWorkListViewDefinition = (WorkListViewDefinition) u1.unmarshal(input);
    			eWorkListViewDefinition.setEffectiveDate(workListView.getEffective_date());
    			eWorkListViewDefinition.setCurrentVersion(workListView.isIs_current_version());
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			eWorkListViewDefinition.setClient(client);
    			client.getWorkListViewDefinition().add(eWorkListViewDefinition);
    			
    			if(eWorkListViewDefinition.getQueryInfos() != null) {
    				eWorkListViewDefinition.getQueryInfos().setWorkListViewDefinition(eWorkListViewDefinition);
    				eWorkListViewDefinition.getQueryInfos().getQueryInfo().forEach(qInfo->{
    					qInfo.setQueryInfos(eWorkListViewDefinition.getQueryInfos());
    					qInfo.getQueryField().forEach(queryField->{
    						queryField.setQueryInfo(qInfo);
    						if(queryField.getOperands()!=null)
    						    queryField.getOperands().setQueryField(queryField);
    						
    					});
    					qInfo.getContent().forEach(content->{
    						content.setQueryInfo(qInfo);
    					});
    				});
    			}
    			
    			if(eWorkListViewDefinition.getSortInfos() != null) {
    				eWorkListViewDefinition.getSortInfos().setWorkListViewDefinition(eWorkListViewDefinition);
    				eWorkListViewDefinition.getSortInfos().getSortInfo().forEach(sInfo->{
    					sInfo.setSortInfos(eWorkListViewDefinition.getSortInfos());
    					sInfo.getContent().forEach(content->{
    						content.setSortInfo(sInfo);
    					});
    					sInfo.getFieldRef().forEach(fRef->{
    						fRef.setSortInfo(sInfo);
    						fRef.getContent().forEach(content->{
    							content.setFieldRef(fRef);
    						});
    					});
    					if(sInfo.getDefaultSortClause()!=null)
    					    sInfo.getDefaultSortClause().setSortInfo(sInfo);
    				});
    			}
    			eWorkListViewDefinition.getFilters().forEach(filters->{
    				filters.setWorkListViewDefinition(eWorkListViewDefinition);
    				filters.getFilter().forEach(filter->{
    					filter.setFilters(filters);
    					filter.getContent().forEach(content->{
    						content.setFilter(filter);
    					});
    				});
    			});
    			
    			eWorkListViewDefinition.getViews().setWorkListViewDefinition(eWorkListViewDefinition);
    			eWorkListViewDefinition.getViews().getView().forEach(view->{
    				view.setViews(eWorkListViewDefinition.getViews());
    				
    				if(view.getFilterRefs() != null) {
    					view.getFilterRefs().setView(view);
    					view.getFilterRefs().transferToEntity();
    					view.getFilterRefs().getFilterRef().forEach(ref->{
    						ref.setFilterRefs(view.getFilterRefs());
    						ref.transferToEntity();
    					});
    				}
    				if(view.getSortInfoRef() != null) {
    					view.getSortInfoRef().setView(view);
    					view.getSortInfoRef().transferToEntity();
    				}
    				
    				if(view.getQueryInfoRef() != null) {
    					view.getQueryInfoRef().setView(view);
    					view.getQueryInfoRef().transferToEntity();
    				}
    				
    				view.getListView().forEach(lView->{
    					lView.setView(view);
    					lView.getFieldRef().forEach(fRef->{
    						fRef.setListView(lView);
    						fRef.getContent().forEach(content->{
    							content.setFieldRef(fRef);
    						});
    					});
    				});
    			});
    			
    			em.persist(client);
    			em.persist(eWorkListViewDefinition);
    			em.flush();
    			em.getTransaction().commit();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(workListView.toString(),exception);
			}finally{
			    em.close();
			}

	}

	/**
	 * Read.
	 *
	 * @param workListViewDefinitionId the work list view definition id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int workListViewDefinitionId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
			WorkListViewDefinition indexMapping = em.find(WorkListViewDefinition.class, workListViewDefinitionId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(indexMapping, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
