/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.menu;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.menudefinition.MenuDefinitionType;
import com.agencyport.configuration.entity.jaxb.menudefinition.ObjectFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The MenuLoader class
 */
@Service
public class MenuLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
	
    final  Logger logger = LoggingManager.getLogger(MenuLoader.class.getName());


	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	
	/**
	 * Load.
	 *
	 * @param client the client
	 * @param menu the menu
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.menu.Menu menu, InputStream input) throws Exception {
			logger.info("Loading :"+menu);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.menudefinition.ObjectFactory.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			JAXBElement<MenuDefinitionType> jaxbMenuDefinitionType = (JAXBElement<MenuDefinitionType>) u1.unmarshal(input);
    			MenuDefinitionType eMenuDefinitionType = jaxbMenuDefinitionType.getValue();
    			eMenuDefinitionType.setEffectiveDate(menu.getEffective_date());
    			eMenuDefinitionType.setCurrentVersion(menu.isIs_current_version());
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			eMenuDefinitionType.setClient(client);
    			client.getMenuDefinitionType().add(eMenuDefinitionType);
    			
    			eMenuDefinitionType.getMenuGroup().forEach(menuGroup -> {
    			    menuGroup.setMenuDefinitionType(eMenuDefinitionType);
    			    menuGroup.getMenuMember().forEach(member ->{
    			        member.setMenuGroupType(menuGroup);
    			        member.getSearchEntry().forEach(searchEntry -> {
    			        	searchEntry.setMenuMemberType(member);
    			        	searchEntry.getSearchToken().forEach(searchToken -> {
    			        		searchToken.setSearchEntryType(searchEntry);
    			        	});
    			        	
    			        });
    			         member.getModal().setMenuMemberType(member);
     			         member.getModal().getButtons().forEach(button ->{
    			        	button.setModal( member.getModal());
    			        });
    			    });
    			    
    			});
    			
    			
    			em.persist(client);
    			em.persist(eMenuDefinitionType);
    			em.flush();
    			em.getTransaction().commit();
    			
			}catch (Exception exception){
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(menu.toString(),exception);
			}finally {
			    em.close();
			}

	}

	
	/**
	 * Read.
	 *
	 * @param menuDefId the menu def id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int menuDefId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.menudefinition.ObjectFactory.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    MenuDefinitionType eMenuDefinitionType = em.find(MenuDefinitionType.class, menuDefId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(new ObjectFactory().createMenuDefinition(eMenuDefinitionType), baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
