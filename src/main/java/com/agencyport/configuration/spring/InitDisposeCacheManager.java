/*
 * Created on Jun 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.spring;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.agencyport.cache.DistributedCacheManagerFactory;
import com.agencyport.cache.IDistributedCacheManager;

/**
 * The InitDisposeCacheManager class
 */
public class InitDisposeCacheManager implements InitializingBean, DisposableBean {

    /** 
     * {@inheritDoc}
     */

    @Override
    public void destroy() throws Exception {
        deleteCache();
    }

    /** 
     * {@inheritDoc}
     */

    @Override
    public void afterPropertiesSet() throws Exception {
        deleteCache();
    }
    
    private void deleteCache() throws Exception{
        String cacheFamily = "configuration";
        @SuppressWarnings("unchecked")
        IDistributedCacheManager<String, String, Object> manager =  DistributedCacheManagerFactory.get();
        manager.delete(cacheFamily);
    	manager.delete("groovy");
    	manager.delete("markdown");
    }

}
