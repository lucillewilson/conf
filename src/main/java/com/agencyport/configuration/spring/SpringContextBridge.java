/*
 * Created on Mar 23, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.spring;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;

import com.agencyport.configuration.repository.HostRepository;
import com.agencyport.configuration.repository.ClientPropertyRepository;
import com.agencyport.configuration.repository.ClientRepository;
import com.agencyport.configuration.repository.ProductRepository;
import com.agencyport.configuration.repository.TransactionRepository;

/**
 * Register this SpringContextBridge as a Spring Component.
 * SpringContextBridge is the bridge to the "non managed" world.
 */
@Repository
public class SpringContextBridge implements SpringContextBridgedServices, ApplicationContextAware {

    /**
     * We put the application context into a static appContextContainer object so that
     * non-Spring managed Java classes can statically get an instance of this bean.
     */
    private static final AppContextContainer APP_CONTEXT_CONTAINER = new AppContextContainer();
    
    @Autowired
    private HealthMonitor healthMonitor;

    /**
     * Called by Spring for all beans which implement {@link ApplicationContextAware}. 
     * We put the application context into a static applicationContext container object so that
     * non-Spring managed Java classes can statically get an instance of this bean.
     * This looks like it's not thread-safe, but it is, as this bean is a singleton.
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        APP_CONTEXT_CONTAINER.setContext(applicationContext);
    }
    
    /**
     * 
     * @return
     */
    public static ApplicationContext getContext(){
        return APP_CONTEXT_CONTAINER.getContext();
    }
    
    /**
     * Get  Spring ContextBridgedServices
     * @return SpringContextBridgedServices
     */
    public static SpringContextBridgedServices services() {
        return APP_CONTEXT_CONTAINER.getContext().getBean(SpringContextBridgedServices.class);
    }
    
    public HealthMonitor getHealthMonitor(){
        return healthMonitor;
    }
    
    /**
     * The <code>client repository</code>.
     */
    @Autowired
    private ClientRepository clientRepository;

    /**
     * The <code>product repository</code>.
     */
    @Autowired
    private ProductRepository productRepository;

    /**
     * The <code>transaction repository</code>.
     */
    @Autowired
    private TransactionRepository transactionRepository;

    /**
     * The <code>entity manager factory</code>.
     */
    @Autowired
    private EntityManagerFactory entityManagerFactory;
    
    /**
     * The <code>host factory</code>.
     */
    @Autowired
    private HostRepository hostRepository;
    
    /**
     * The <code>client property factory</code>.
     */
    @Autowired
    private ClientPropertyRepository clientPropertyRepository;
    
    /**
     * {@inheritDoc}
     */

    @Override
    public ClientRepository getClientRepository() {
        return clientRepository;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public ProductRepository getProductRepository() {
        return productRepository;
    }

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }
    
    /**
	 * HostRepository
	 * @return HostRepository
	 */
    @Override
	public HostRepository getHostRepository() {
		return hostRepository;
	}

    /** 
     * {@inheritDoc}
     */ 
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
    
    @Override
    public ClientPropertyRepository getClientPropertyRepository() {
		return clientPropertyRepository;
	}

	public void setClientPropertyRepository(ClientPropertyRepository clientPropertyRepository) {
		this.clientPropertyRepository = clientPropertyRepository;
	}

	/**
     * We put the application context into a static appContextContainer object so that
     * non-Spring managed Java classes can statically get an instance of this bean.
     * @author ahayes
     */
    private static class AppContextContainer {
        private ApplicationContext applicationContext;

        public void setContext(ApplicationContext applicationContext) {
            this.applicationContext = applicationContext;
        }
        public ApplicationContext getContext() {
            return applicationContext;
        }
    }

}
