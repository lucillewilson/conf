/*
 * Created on Apr 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.rest.AtomLink;


/**
 * The Class OptionList.
 *
 * @param <T> the generic type
 */
@XmlRootElement(name = "optionList")
public class OptionList<T> {
    
    /**
     * The <code>option</code>.
     */
    @XmlElement(required = true)
    protected List<T> option;
    
    /**
     * The <code>link</code>.
     */
    @XmlElement(required = true)
    private List<AtomLink> link;

    /**
     * Gets the option.
     *
     * @return the option
     */
    public List<T> getOption() {
        if (option == null) {
            option = new ArrayList<T>();
        }
        return this.option;
    }

    /**
     * Gets the link.
     *
     * @return the link
     */
    public List<AtomLink> getLink() {
        if(link == null) {
            link = new ArrayList<AtomLink>();
        }
        return link;
    }

}
