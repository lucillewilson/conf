/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.product.outputter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow;
import com.agencyport.configuration.entity.jaxb.xarcrules.Rule;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.forwardwriter.IForwardWriter;

/**
 * The RuleOutputter class
 */
public class RuleOutputter extends StandardOutputter {
    /**
     * The <code>rules</code> is the list Rule object that this
     * out putter interested in.
     */
    private List<Rule> rules = new ArrayList<Rule>();

    /**
     * 
     * Constructs an instance.
     * 
     * @param mediaType
     *            is the media type to out put
     *            {@link javax.ws.rs.core.MediaType}
     * @param rules
     *            RuleFile
     */
    public RuleOutputter(MediaType mediaType, List<Rule> rules) {
        super(mediaType);
        this.rules = rules;
    }

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    protected void renderResults(IForwardWriter fw, StandardResponseEntity entity) throws IOException {
        fw.writeObjectFieldStart(StandardResponseEntity.RESULTS_TAG);

        if (getMediaType().equals(MediaType.APPLICATION_JSON_TYPE)) {
            fw.writeObjectField("workflows", getOutput());
        } else {
            fw.writeObjectField(null, getOutput());
        }
        fw.writeEndObject();

    }

    /**
     * Get getOutput
     * 
     * @return JDOM document representing {@link TWorkFlow}
     * @throws IOException
     *             when io exception
     */
    protected Document getOutput() throws IOException {

        try {

            StringBuilder buffer = new StringBuilder();
            buffer.append("<rules>");

            JAXBContext context = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.xarcrules.Rule.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            rules.forEach(rule -> {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {

                    marshaller.marshal(rule, baos);
                } catch (JAXBException e) {
                    ExceptionLogger.log(e, getClass(), "getOutput");
                }
                buffer.append(new String(baos.toByteArray()));
            });

            buffer.append("</rules>");

            SAXBuilder builder = new SAXBuilder();
            return builder.build(new ByteArrayInputStream(buffer.toString().getBytes()));

        } catch (JDOMException | JAXBException exception) {
            throw new IOException(exception);

        }

    }

}
