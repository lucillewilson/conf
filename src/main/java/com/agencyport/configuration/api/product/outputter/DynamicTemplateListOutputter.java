/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.product.outputter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.ObjectFactory;
import com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.TDynamicListTemplate;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.forwardwriter.IForwardWriter;

/**
 * The DynamicTemplateListOutputter class
 */
public class DynamicTemplateListOutputter extends StandardOutputter {
    /**
     * The <code>dynamicListTemplateList</code> is the list TDynamicListTemplate object that this
     * out putter interested in.
     */
    private List<TDynamicListTemplate> dynamicListTemplateList = new ArrayList<TDynamicListTemplate>();

    /**
     * 
     * Constructs an instance.
     * 
     * @param mediaType
     *            is the media type to out put
     *            {@link javax.ws.rs.core.MediaType}
     * @param dynamicListTemplateList
     *            TDynamicListTemplate
     */
    public DynamicTemplateListOutputter(MediaType mediaType, List<TDynamicListTemplate> dynamicListTemplateList) {
        super(mediaType);
        this.dynamicListTemplateList = dynamicListTemplateList;
    }

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    protected void renderResults(IForwardWriter fw, StandardResponseEntity entity) throws IOException {
        fw.writeObjectFieldStart(StandardResponseEntity.RESULTS_TAG);

        if (getMediaType().equals(MediaType.APPLICATION_JSON_TYPE)) {
            fw.writeObjectField("dynamicListTemplateLists", getOutput());
        } else {
            fw.writeObjectField(null, getOutput());
        }
        fw.writeEndObject();

    }

    /**
     * Get getOutput
     * 
     * @return JDOM document representing {@link TDynamicListTemplate}
     * @throws IOException
     *             when io exception
     */
    protected Document getOutput() throws IOException {

        try {

            StringBuilder buffer = new StringBuilder();
            buffer.append("<dynamicListTemplateLists>");

            JAXBContext context = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.TDynamicListTemplate.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            dynamicListTemplateList.forEach(dynamicListTemplate -> {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {

                    marshaller.marshal(new ObjectFactory().createDynamicListTemplate(dynamicListTemplate), baos);
                } catch (JAXBException e) {
                    ExceptionLogger.log(e, getClass(), "getOutput");
                }
                buffer.append(new String(baos.toByteArray()));
            });

            buffer.append("</dynamicListTemplateLists>");

            SAXBuilder builder = new SAXBuilder();
            return builder.build(new ByteArrayInputStream(buffer.toString().getBytes()));

        } catch (JDOMException | JAXBException exception) {
            throw new IOException(exception);

        }

    }

}
