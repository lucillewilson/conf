/*
 * Created on Jun 25, 2016 by pnamepally AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.menu;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.agencyport.configuration.entity.jaxb.menudefinition.MenuDefinitionType;
import com.agencyport.configuration.entity.jaxb.menudefinition.MenuGroupType;
import com.agencyport.configuration.entity.jaxb.menudefinition.ObjectFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.forwardwriter.IForwardWriter;

/**
 * The WorkFlowOutputter class
 */
public class MenuOutputter extends StandardOutputter {
    /**
     * The <code>workFlows</code> is the list TWorkFlow object that this
     * out putter interested in.
     */
    private List<MenuGroupType> menuGroup = new ArrayList<MenuGroupType>();

    /**
     * 
     * Constructs an instance.
     * 
     * @param mediaType
     *            is the media type to out put
     *            {@link javax.ws.rs.core.MediaType}
     * @param menus
     *            MenuDefinitionType
     */
    public MenuOutputter(MediaType mediaType, List<MenuGroupType> menus) {
        super(mediaType);
        this.menuGroup = menus;
    }

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    protected void renderResults(IForwardWriter fw, StandardResponseEntity entity) throws IOException {
        fw.writeObjectFieldStart(StandardResponseEntity.RESULTS_TAG);

        if (getMediaType().equals(MediaType.APPLICATION_JSON_TYPE)) {
            fw.writeObjectField("menudefinition", getOutput());
        } else {
            fw.writeObjectField(null, getOutput());
        }
        fw.writeEndObject();

    }

    /**
     * Get getOutput
     * 
     * @return JDOM document representing {@link MenuDefinitionType}
     * @throws IOException
     *             when io exception
     */
    protected Document getOutput() throws IOException {

        try {

            StringBuilder buffer = new StringBuilder();
            buffer.append("<menuDefinition>");

            JAXBContext context = JAXBContext.newInstance(MenuGroupType.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            menuGroup.forEach(menuGroup -> {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                try {
	            		marshaller.marshal(new ObjectFactory().createMenuGroup(menuGroup) , baos);
	                } catch (JAXBException e) {
	                    ExceptionLogger.log(e, getClass(), "getOutput");
	                }
                buffer.append(new String(baos.toByteArray()));
            });

            buffer.append("</menuDefinition>");

            SAXBuilder builder = new SAXBuilder();
            return builder.build(new ByteArrayInputStream(buffer.toString().getBytes()));

        } catch (JDOMException | JAXBException exception) {
            throw new IOException(exception);

        }

    }

}
