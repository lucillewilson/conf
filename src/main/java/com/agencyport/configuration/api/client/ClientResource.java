package com.agencyport.configuration.api.client;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.outputter.PojoContainerOutputter;
import com.agencyport.api.outputter.pojo.PojoContainer;
import com.agencyport.api.outputter.pojo.PojoObjectContainer;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.clientproperty.Property;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.configuration.spring.SpringContextBridgedServices;
import com.agencyport.rest.Status;

/**
 * The ClientResource class expresses the client entity as a resource.
 */
@Path("/clients")
public class ClientResource extends BaseResource {
	
	/**
	 * Returns the client entity associated with the given virtual hostname along with the client_properties
	 * and other client related information
	 * @param hostname the virtual hostname e.g. 'dev.wayne.agencyport.com'
	 * @return the client entity
	 */
    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Path("/{hostname}")
    public Response getClient(@PathParam("hostname") String hostname) {
        try {  
        	SpringContextBridgedServices Services = SpringContextBridge.services();
			Integer clientId = Services.getHostRepository().findClientIdByGatewayHost(hostname);
			if(clientId == null){
            	return this.generateExceptionResponse(getAccepts(), new SecurityException("Could not retrieve client for host domain"), Status.REQUEST_VERIFICATION_FAILURE, "getClient", Response.Status.BAD_REQUEST);
            }
			
			Client client = Services.getClientRepository().findById(clientId);
			List<Property> properties = client.getClientProperty();
			
			properties = Services.getClientPropertyRepository().findByClientId(clientId);
			client.setClientProperty(properties);
			
            PojoContainer pojoContainer = new PojoObjectContainer("client", client); 
            return generateResponse(getAccepts(), new PojoContainerOutputter(getAccepts(), pojoContainer));
        } catch (final RuntimeException exception) {
            return this.generateExceptionResponse(getAccepts(), exception, Status.AUTHENTICATION_FAILURE, "getClient", Response.Status.BAD_REQUEST);
        }
    }
}