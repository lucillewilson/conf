/*
 * Created on Apr 12, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import java.io.Serializable;

import javax.persistence.AttributeConverter;

/**
 * The OpenModeConverter class
 */
public class OpenModeConverter implements AttributeConverter<TOpenMode , String> ,Serializable {
    
    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = -1392015353139279770L;

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String convertToDatabaseColumn(TOpenMode attribute) {
        if(attribute==null)
            return null;
        
        switch (attribute) {
            case EDIT_MODE:
                return "editMode";
            case NO:
                return "no";
            case SUMMARY_ONLY:
                return "summaryOnly";
            case VIEW_MODE:
                return "viewMode";
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public TOpenMode convertToEntityAttribute(String dbData) {
        if(dbData==null){
            return null;
        }
        switch (dbData) {
            case "editMode":
                return TOpenMode.EDIT_MODE;
            case "no":
                return TOpenMode.NO;
            case "summaryOnly":
                return TOpenMode.SUMMARY_ONLY;
            case "viewMode":
                return TOpenMode.VIEW_MODE;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }

}
