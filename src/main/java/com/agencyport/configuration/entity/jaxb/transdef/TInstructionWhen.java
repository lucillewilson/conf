package com.agencyport.configuration.entity.jaxb.transdef;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The Class TInstructionWhen.
 */
@Table(name = "tdf_instruction_when" )
@Entity(name = "tdf_instruction_when")
@NamedQuery(name = "tdf_instruction_when.findAll", query = "SELECT a FROM tdf_instruction_when a")
public class TInstructionWhen {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>instruction</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private TInstruction instruction;

    /**
     * The <code>connector</code>.
     */
    @XmlTransient
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "connector_auto_id", insertable = true, updatable = true, nullable = true, unique = false)
    private TConnector connector;

    /**
     * Gets the value of the connector property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public Object getConnector() {
        return connector;
    }

    /**
     * Sets the value of the connector property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setConnector(Object value) {
        this.connector = (TConnector) value;
    }

    /**
     * Gets the instruction.
     *
     * @return the instruction
     */
    public TInstruction getInstruction() {
        return instruction;
    }

    /**
     * Sets the instruction.
     *
     * @param instruction the new instruction
     */
    public void setInstruction(TInstruction instruction) {
        this.instruction = instruction;
    }

    /**
     * Gets the tcconnector.
     *
     * @return the tcconnector
     */
    public TConnector getTcconnector() {
        return connector;
    }

    /**
     * Sets the tcconnector.
     *
     * @param tcconnector the new tcconnector
     */
    public void setTcconnector(TConnector tcconnector) {
        this.connector = tcconnector;
    }

    /**
     * Get Auto generated ID
     * 
     * @return autoId
     */
    public int getAutoId() {
        return autoId;
    }

}
