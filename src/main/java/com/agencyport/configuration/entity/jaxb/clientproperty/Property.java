package com.agencyport.configuration.entity.jaxb.clientproperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.agencyport.configuration.entity.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name = "Property")
@Table(name = "client_property", 
	uniqueConstraints = @UniqueConstraint(columnNames = {"auto_id", "client_id", "env", "property_key", "property_value", "encrypted"}))
public class Property {
	
	@Id
    @Column(name = "auto_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int auto_id;
	
	@Column(name = "env")
	private String env;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable=true, name="client_id")
    @JsonIgnore
	private Client client;
	
	@XmlAttribute
	@Column(name = "property_key")
	private String key;
	
	@XmlAttribute
	@Column(name = "property_value")
	private String value;
	
	@XmlAttribute
	@Column(name = "encrypted")
	private boolean encrypted;
	
	public boolean getEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getAuto_id() {
		return auto_id;
	}

	public void setAuto_id(int auto_id) {
		this.auto_id = auto_id;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
