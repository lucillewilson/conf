//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.24 at 07:33:30 PM EDT 
//


package com.agencyport.configuration.entity.jaxb.workitemassistant;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.agencyport.configuration.entity.helper.ListToStringConverter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="role" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}codeListRef" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="comments"/>
 *             &lt;enumeration value="fileAttachments"/>
 *             &lt;enumeration value="activeUsers"/>
 *             &lt;enumeration value="history"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "role",
    "codeListRef"
})
@XmlRootElement(name = "section")
@Entity(name = "workitemassistant_section")
@Table(name = "workitemassistant_section", uniqueConstraints = @UniqueConstraint(columnNames = { "workitemassistant_auto_id","id" }))
@NamedQuery(name = "workitemassistant_section.findAll", query = "SELECT a FROM workitemassistant_section a")
public class Section {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>role</code>.
     */
    @Column(name = "role")
    @Convert(converter=ListToStringConverter.class)
    protected List<String> role;
    
    /**
     * The <code>code list ref</code>.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "section", cascade = CascadeType.ALL)
    protected List<CodeListRef> codeListRef;
    
    /**
     * The <code>type</code>.
     */
    @Column(name = "type")
    @XmlAttribute(name = "type", required = true)
    protected String type;
    
    /**
     * The <code>id</code>.
     */
    @Column(name = "id")
    @XmlAttribute(name = "id", required = true)
    protected String id;
    
    /**
     * The <code>label</code>.
     */
    @Column(name = "label")
    @XmlAttribute(name = "label", required = true)
    protected String label;
    
    /**
     * The <code>client</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Workitemassistant workitemassistant;

    /**
     * Gets the value of the role property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the role property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRole() {
        if (role == null) {
            role = new ArrayList<String>();
        }
        return this.role;
    }

    /**
     * Gets the value of the codeListRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codeListRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodeListRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodeListRef }
     * 
     * 
     */
    public List<CodeListRef> getCodeListRef() {
        if (codeListRef == null) {
            codeListRef = new ArrayList<CodeListRef>();
        }
        return this.codeListRef;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Returns the autoId	
     * @return the autoId
     */
    public int getAutoId() {
        return autoId;
    }

    /**
     * Returns the workitemassistant	
     * @return the workitemassistant
     */
    public Workitemassistant getWorkitemassistant() {
        return workitemassistant;
    }

    /**
     * Sets the workitemassistant to workitemassistant
     * @param workitemassistant the workitemassistant to set
     */
    public void setWorkitemassistant(Workitemassistant workitemassistant) {
        this.workitemassistant = workitemassistant;
    }

}
