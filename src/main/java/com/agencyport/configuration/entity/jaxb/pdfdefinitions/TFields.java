//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.15 at 07:59:51 AM EDT 
//


package com.agencyport.configuration.entity.jaxb.pdfdefinitions;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


/**
 * Type definition for field aggregate, usually defined in overflow and included fields
 * 
 * <p>Java class for T_fields complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="T_fields">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}field" maxOccurs="unbounded"/>
 *         &lt;element ref="{}checkBoxGroup" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_fields", propOrder = {
    "field",
    "checkBoxGroup"
})
@Entity(name = "pdfdefinitions_fields")
@Table(name = "pdfdefinitions_fields")
@NamedQuery(name = "pdfdefinitions_fields.findAll", query = "SELECT a FROM pdfdefinitions_fields a")
public class TFields {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fields", cascade = CascadeType.ALL)
    @XmlElement(required = true)
    protected List<TField> field;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fields", cascade = CascadeType.ALL)
    protected List<TCheckBoxGroup> checkBoxGroup;
    
    @XmlTransient
    @ManyToMany(fetch = FetchType.LAZY)
    private List<TForm> form;
    
    @XmlTransient
    @ManyToMany(fetch = FetchType.LAZY)
    private List<TSchedule> schedule;
    
    @XmlTransient
    @ManyToMany(fetch = FetchType.LAZY)
    private List<TOverflow> overflow;
    
    @XmlTransient
    @ManyToMany(fetch = FetchType.LAZY)
    private List<TRepeat> repeat;
    
    
   

    /**
     * Gets the value of the field property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the field property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TField }
     * 
     * 
     */
    public List<TField> getField() {
        if (field == null) {
            field = new ArrayList<TField>();
        }
        return this.field;
    }

    /**
     * Gets the value of the checkBoxGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the checkBoxGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCheckBoxGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TCheckBoxGroup }
     * 
     * 
     */
    public List<TCheckBoxGroup> getCheckBoxGroup() {
        if (checkBoxGroup == null) {
            checkBoxGroup = new ArrayList<TCheckBoxGroup>();
        }
        return this.checkBoxGroup;
    }

    /**
     * Returns the autoId	
     * @return the autoId
     */
    public int getAutoId() {
        return autoId;
    }

    /**
     * Returns the form	
     * @return the form
     */
    public List<TForm> getForm() {
        if(form == null){
            form = new ArrayList<TForm>();
        }
        return form;
    }

    /**
     * Returns the schedule	
     * @return the schedule
     */
    public List<TSchedule> getSchedule() {
        if(schedule ==null){
            schedule = new ArrayList<TSchedule>();
        }
        return schedule;
    }

    /**
     * Returns the repeat	
     * @return the repeat
     */
    public List<TRepeat> getRepeat() {
        if(repeat ==null){
            repeat = new ArrayList<TRepeat>();
        }
        return repeat;
    }
    
    /**
     * Returns the overflow	
     * @return the overflow
     */
    public List<TOverflow> getOverflow() {
        if(overflow ==null){
            overflow = new ArrayList<TOverflow>();
        }
        return overflow;
    }

    @PrePersist
    public void transferToEntity() {
        getField().forEach(f->{
            f.setFields(this);
        });
        getCheckBoxGroup().forEach(ck->{
            ck.setFields(this);
        });
    }
    @PostLoad
    public void transferFromEntity() {
        
    }
}
