package com.agencyport.configuration.entity.jaxb.clientproperty;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ClientProperties{
	
	@XmlElement(name="clientProperty")
	private List<ClientProperty> clientProperty;

	public List<ClientProperty> getProperties() {
		return clientProperty;
	}

	public void setProperties(List<ClientProperty> clientProperties) {
		this.clientProperty = clientProperties;
	}
}
