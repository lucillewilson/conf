package com.agencyport.configuration.entity.jaxb.hosts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import com.agencyport.configuration.entity.Client;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name = "host")
@Table(name = "host", 
	uniqueConstraints = @UniqueConstraint(columnNames = {"auto_id", "client_id", "gateway_host"}))
public class Host {
	
	@Id
    @Column(name = "auto_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable=false, name="client_id")
    @XmlTransient
	private Client client;
	
	@Column(name="gateway_host")
	@XmlAttribute(name="name")
	private String gatewayHost;
	
	public int getAuto_id() {
		return id;
	}

	public void setAuto_id(int auto_id) {
		this.id = auto_id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getGatewayHost() {
		return gatewayHost;
	}

	public void setGatewayHost(String gatewayHost) {
		this.gatewayHost = gatewayHost;
	}
}
