/**
 * @author scottsurette
 */

package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>Java class for T_reason complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 *  <xs:complexType name="T_reason">
 *    <xs:simpleContent>
 *      <xs:extension base="xs:string">
 *        <xs:attribute name="value" type="xs:string" use="required" />
 *      </xs:extension>
 *    </xs:simpleContent>
 *  </xs:complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_reason")
@Entity(name = "work_flow_reason")
@Table(name = "work_flow_reason" )
@NamedQuery(name = "work_flow_reason.findAll", query = "SELECT a FROM work_flow_reason a")
public class TReason {

	/**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;
    
    /**
     * The <code>value</code>.
     */
    @Column(name = "value")
    @XmlAttribute(name = "value", required = true)
    protected String value;
    
    /**
     * The String representation of <code>reason</code>
     */
    @Column(name = "reason")
    @XmlValue
    protected String reason;
    
    /**
     * The <code>reasonOptions</code>.
     */
    @XmlTransient
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn
    private TReasonOptions reasonOptions;

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the reasonOptions
	 */
	public TReasonOptions getReasonOptions() {
		return reasonOptions;
	}

	/**
	 * @param reasonOptions the reasonOptions to set
	 */
	public void setReasonOptions(TReasonOptions reasonOptions) {
		this.reasonOptions = reasonOptions;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
}
