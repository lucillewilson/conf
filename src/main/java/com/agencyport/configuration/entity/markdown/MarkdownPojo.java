/*
 * Created on Jan 26, 2017 by btran AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.markdown;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This models the markdown file entity. 
 * 
 *	@author btran
 */

@Entity
@Table(name="markdown", uniqueConstraints = @UniqueConstraint(columnNames = { "id", "markdown_name", "markdown_file", "client_Id", "product_Id"}))
public class MarkdownPojo {
	
	@Id
    @Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "markdown_name")
	private String markdown_name;
	
	@Column(name = "markdown_file")
	private String markdown_file;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable=true, name= "client_Id")
    @JsonIgnore
	private Client client;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable=true, name = "product_Id")
    @JsonIgnore
	private Product product;
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getmarkdownFile() {
		return markdown_file;
	}

	public void setmarkdownFile(String markdownFile) {
		this.markdown_file = markdownFile;
	}

	public String getmarkdownName() {
		return markdown_name;
	}

	public void setmarkdownName(String markdownName) {
		this.markdown_name = markdownName;
	}
	
	public Client getClient(){
		return client;
	}
	
	public void setClient(Client client){
		this.client = client;
	}
	
	public Product getProduct(){
		return product;
	}
	
	public void setProduct(Product product){
		this.product = product;
	}
}
