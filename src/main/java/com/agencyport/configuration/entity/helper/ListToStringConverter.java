/*
 * Created on Apr 12, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;

/**
 * The ListToStringConverter class
 */
public class ListToStringConverter implements AttributeConverter<List<String> , String>,Serializable {
    
    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = -5233011352915176352L;

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        if(attribute==null){
            return null;
        }
        StringBuilder buffer = new StringBuilder();
        attribute.forEach(op -> {
            buffer.append(";").append(op);
        });
        String str =buffer.toString().substring(";".length());
        return str;
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        List<String> returnVal = new ArrayList<String>();
        if (dbData != null) {
            Arrays.asList(dbData.split(";")).forEach(o -> {
                returnVal.add(o);
            });
        }
        if(returnVal.isEmpty()){
            return null;
        }
        return returnVal;
    }

}
