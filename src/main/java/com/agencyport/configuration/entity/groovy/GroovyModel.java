package com.agencyport.configuration.entity.groovy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This models the groovy file entity. 
 * 
 *	@author btran
 */

@Entity(name = "groovy")
@Table(name="groovy", uniqueConstraints = @UniqueConstraint(columnNames = {"id", "groovy_name", "groovy_file", "client_Id", "product_Id"}))
public class GroovyModel {
	
	@Id
    @Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "groovy_name")
	private String groovy_name;
	
	@Column(name = "groovy_file")
	private String groovy_file;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable=true, name="client_Id")
    @JsonIgnore
	private Client client;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable=true, name= "product_Id")
    @JsonIgnore
    private Product product;
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroovyFile() {
		return groovy_file;
	}
	
	public Product getProduct(){
		return product;
	}
	
	public void setProduct(Product product){
		this.product = product;
	}

	public void setGroovyFile(String groovyFile) {
		this.groovy_file = groovyFile;
	}

	public String getGroovyName() {
		return groovy_name;
	}

	public void setGroovyName(String groovyName) {
		this.groovy_name = groovyName;
	}
	
	public Client getClient(){
		return client;
	}
	
	public void setClient(Client client){
		this.client = client;
	}
}
