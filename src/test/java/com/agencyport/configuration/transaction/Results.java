/*
 * Created on Apr 27, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.jdom2.Element;


/**
 * The Results class
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="results")
@XmlType(name = "", propOrder = {
        "object"
})
public class Results {
    
    /**
     * The <code>object</code>.
     */
    @XmlAnyElement(value=JDomHandler.class)
    private Element object;

    /**
     * Returns the object	
     * @return the object
     */
    public Element getObject() {
        return object;
    }

    /**
     * Sets the object to object
     * @param object the object to set
     */
    public void setObject(Element object) {
        this.object = object;
    }

}
