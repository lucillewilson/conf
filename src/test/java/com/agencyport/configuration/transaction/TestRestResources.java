/*
 * Created on Mar 31, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.transaction;

import java.io.StringReader;
import java.net.URI;
import java.util.List;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.agencyport.configuration.spring.SpringApplicationInitializer;
import com.agencyport.configuration.spring.SpringApplicationInitializer.SdkTestContextInitializer;
import com.agencyport.configuration.spring.TestConfig;

/**
 * The TestTransaction class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={SpringApplicationInitializer.class, TestConfig.class}, loader=AnnotationConfigWebContextLoader.class, initializers=SdkTestContextInitializer.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRestResources extends LoadProductDef{
	
	/**
	 * The <code>context</code>.
	 */
	@Autowired
	ApplicationContext context;
	
	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;
	

    /**
     * Test Products.
     */
    @Test
    public void testProducts() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/products")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Products status is not success", Integer.parseInt(response.getStatus()) > 0);
        List<Element> elements =  response.getResults().getObject().getChildren();
        for(Element element : elements){
            String url = getUrl(element, "transactions");
            getTranAndBehaviors(url);
        }
        
        
        
    }
    
    public void getTranAndBehaviors( String url) throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");
        url =getUrl(url);
        WebTarget target= test.target(url);
        for (NameValuePair param : params) {
            target = target.queryParam(param.getName(), param.getValue());
          }
        String strResponse  =    target.request(MediaType.APPLICATION_XML_TYPE)
              .get(String.class);
       Response transactionOrBehaviorResponse =  getResponse(strResponse);
       Assert.assertNotNull ("TranAndBehaviors Response status is not success", Integer.parseInt(transactionOrBehaviorResponse.getStatus()) > 0);
       
       List<Element> elements =  transactionOrBehaviorResponse.getResults().getObject().getChildren();
       for(Element element : elements){
               String tUrl = getUrl(element, "self");
               if(!tUrl.isEmpty())
                   getTranAndBehaviors(tUrl);
               
               String bUrl = getUrl(element, "behaviors");
               if(!bUrl.isEmpty())
                   getTranAndBehaviors(bUrl);
           
       }
        
    }
    
    /**
     * Test Xarcrules.
     */
    @Test
    public void testXarcrules() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/xarcrules")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
         
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        List<Element> elements =   response.getResults().getObject().getChildren();
        for(Element element : elements){
            String url = getUrl(element, "self");
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");
            url =getUrl(url);
            WebTarget target= test.target(url);
            for (NameValuePair param : params) {
                target = target.queryParam(param.getName(), param.getValue());
              }
           strResponse =    target.request(MediaType.APPLICATION_XML_TYPE)
                  .get(String.class);
           Response vResponse =  getResponse(strResponse);
           Assert.assertNotNull ("Xarcrules Response status is not success", Integer.parseInt(vResponse.getStatus()) > 0);
            
        }
    }
    
    /**
     * Test Views.
     */
    @Test
    public void testViews() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/views")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        List<Element> elements =  response.getResults().getObject().getChildren();
        
        for(Element element : elements){
            int size = element.getChildren().size();
            Element last=  element.getChildren().get(size-1);
            String url = last.getAttributeValue("href");
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");
            url =getUrl(url);
            WebTarget target= test.target(url);
            for (NameValuePair param : params) {
                target = target.queryParam(param.getName(), param.getValue());
              }
           strResponse =    target.request(MediaType.APPLICATION_XML_TYPE)
                  .get(String.class);
           Response vResponse =  getResponse(strResponse);
           Assert.assertNotNull ("View Response status is not success", Integer.parseInt(vResponse.getStatus()) > 0);
            
        }
        

    }
    
    /**
     * Test Dynamiclisttemplates.
     */
    @Test
    public void testDynamiclisttemplates() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/dynamiclisttemplates")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        List<Element> elements =   response.getResults().getObject().getChildren();
        
        for(Element element : elements){
            int size = element.getChildren().size();
            Element last=  element.getChildren().get(size-1);
            String url = last.getAttributeValue("href");
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");
            url =getUrl(url);
            WebTarget target= test.target(url);
            for (NameValuePair param : params) {
                target = target.queryParam(param.getName(), param.getValue());
              }
           strResponse =    target.request(MediaType.APPLICATION_XML_TYPE)
                  .get(String.class);
           Response vResponse =  getResponse(strResponse);
           Assert.assertNotNull ("Dynamiclisttemplates Response status is not success", Integer.parseInt(vResponse.getStatus()) > 0);
            
        }
        

    }
    
    /**
     * Test Optionlists.
     */
    @Test
    public void testOptionlists() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/optionlists")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        List<Element> elements =   response.getResults().getObject().getChildren();
        
        for(Element element : elements){
            int size = element.getChildren().size();
            Element last=  element.getChildren().get(size-1);
            String url = last.getAttributeValue("href");
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");
            url =getUrl(url);
            WebTarget target= test.target(url);
            for (NameValuePair param : params) {
                target = target.queryParam(param.getName(), param.getValue());
              }
           strResponse =    target.request(MediaType.APPLICATION_XML_TYPE)
                  .get(String.class);
           Response vResponse =  getResponse(strResponse);
           Assert.assertNotNull ("Optionlists Response status is not success", Integer.parseInt(vResponse.getStatus()) > 0);
            
        }
        

    }
    
    /**
     * Test WCClassCodes.
     */
    @Test
    public void testWCClassCodes() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/wcclasscode")
                
                .queryParam("stateprovcd", "CA")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        strResponse = 
                test.target("productdefinition/wcclasscode")
                
                .queryParam("stateprovcd", "NH")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        strResponse = 
                test.target("productdefinition/wcclasscode")
                
                .queryParam("stateprovcd", "NH")
                .queryParam("classcodetype", "USLH")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);

    }
    
    /**
     * Test IndustryCodes.
     */
    @Test
    public void testIndustryCodes() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("productdefinition/industrycode")
                
                .queryParam("industrycdtype", "NAICS")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        strResponse = 
                test.target("productdefinition/industrycode")
                
                .queryParam("industrycdtype", "SIC")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        response =  getResponse(strResponse);
        Assert.assertNotNull ("Response status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        

    }
	
    public Response getResponse(String strResponse)  throws Exception{
        JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(strResponse);
        return (Response) unmarshaller.unmarshal(reader);
    }
    
    public String getUrl(Element parent, String rel ) {
        
        List<Element> elements = parent.getChildren();
        for(Element element : elements){
            if("link".equals(element.getName())){
                List<Attribute> attributes = element.getAttributes();
                for(Attribute attr : attributes) {
                    if(rel.equals(attr.getValue())){
                        if(element.getAttributeValue("href") != null) {
                            return element.getAttributeValue("href");
                        }else {
                             ;
                        }
                    }
                }
            }
        }
        
        return "";
        
        
    }
    
    /**
     * Test WorkFlow.
     */
    @Test
    public void testWorkFlows() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("workflow/AGENT")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Products status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        
        
    }
    
    /**
     * Test SolrIndexMapping.
     */
    @Test
    public void testSolrIndexMapping() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("indexmapping")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Products status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        
        
    }
    
    /**
     * Test WorkListViewDefinition.
     */
    @Test
    public void testWorkListViewDefinition() throws Exception{
        JerseyTest test = (JerseyTest)context.getBean("jerseyTest");
        String strResponse = 
                test.target("worklistview")
                
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(String.class);
        
        Response response =  getResponse(strResponse);
        Assert.assertNotNull ("Products status is not success", Integer.parseInt(response.getStatus()) > 0);
        
        
        
    }
    
    private String getUrl(String url) {
        
        url = url.substring("api/".length());
        if(url.indexOf("?") > 0)
            url =url.substring(0, url.indexOf("?"));
        
        return url;
        
    }
	
	

}
