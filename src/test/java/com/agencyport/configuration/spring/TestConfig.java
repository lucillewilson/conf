/*
 * Created on Mar 31, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.spring;

import javax.sql.DataSource;

import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.ServletDeploymentContext;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.agencyport.configuration.loader.BatchLoader;
import com.agencyport.configuration.loader.acordtoworkitem.AcordToWorkItemLoader;
import com.agencyport.configuration.loader.behavior.TDBehaviorLoader;
import com.agencyport.configuration.loader.dynamiclist.DynamicTemplateListLoader;
import com.agencyport.configuration.loader.indexmapping.IndexMappingLoader;
import com.agencyport.configuration.loader.industrycode.IndustryCodeLoader;
import com.agencyport.configuration.loader.menu.MenuLoader;
import com.agencyport.configuration.loader.optionlist.OptionListLoader;
import com.agencyport.configuration.loader.pdf.PdfdefinitionsLoader;
import com.agencyport.configuration.loader.transdef.TDLoader;
import com.agencyport.configuration.loader.transformer.TransformerLoader;
import com.agencyport.configuration.loader.view.ViewLoader;
import com.agencyport.configuration.loader.wcclasscode.WorkCompClassCodeLoader;
import com.agencyport.configuration.loader.workflow.WorkFlowLoader;
import com.agencyport.configuration.loader.workitemassistant.WorkItemAssistantLoader;
import com.agencyport.configuration.loader.worklistview.WorkListViewLoader;
import com.agencyport.configuration.loader.xarcrules.XarcRulesLoader;

/**
 * The TestConfig class
 */
@Configuration
@Profile(SpringApplicationInitializer.TEST_PROFILE)
public class TestConfig {
	
	/**
	 * The <code>environment</code>.
	 */
	@Autowired
    private Environment environment; 
    
	/**
	 * Gets the test data source.
	 *
	 * @return the test data source
	 */
	@Bean(name = "dataSource")
    public DataSource getTestDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        
        dataSource.setDriverClassName(environment.getProperty("jdbc_driver"));
        dataSource.setUrl(environment.getProperty("database_url"));
        dataSource.setUsername("configuration");
        dataSource.setPassword("configuration");
        return dataSource;
    }
	
	/**
	 * Gets the jersey test.
	 *
	 * @return the jersey test
	 * @throws Exception the exception
	 */
	@Bean(name = "jerseyTest")
	public JerseyTest getJerseyTest() throws Exception{
	    
	    JerseyTest jerseyTest = new JerseyTest(new GrizzlyWebTestContainerFactory()){
	        
	        @Override
	        protected DeploymentContext configureDeployment() {
	            enable(TestProperties.LOG_TRAFFIC);
	            enable(TestProperties.DUMP_ENTITY);
	            return ServletDeploymentContext.forPackages("com.agencyport.configuration.api").contextPath("/api").build();
	        }
	        
	    };
	    jerseyTest.setUp();
	    return jerseyTest;
	    
	}
	
	/**
	 * Dispose test resource.
	 *
	 * @param jerseyTest the jersey test
	 * @return the disposable bean
	 */
	@Bean(name="disposeResource")
    public DisposableBean disposeTestResource(JerseyTest jerseyTest) {
        return new DisposableBean() {
            @Override
            public void destroy() throws Exception {
                jerseyTest.tearDown();
                
            }
        };
        
    }
	
	/**
	 * Creates the td loader.
	 *
	 * @return the TD loader
	 */
	@Bean
    public TDLoader createTDLoader() {
        return new TDLoader();
    }
    
    /**
     * Creates the td behavior loader.
     *
     * @return the TD behavior loader
     */
    @Bean
    public TDBehaviorLoader createTDBehaviorLoader() {
        return new TDBehaviorLoader();
    }
    
    /**
     * Creates the index mapping loader.
     *
     * @return the index mapping loader
     */
    @Bean
    public IndexMappingLoader createIndexMappingLoader() {
        return new IndexMappingLoader();
    }
    
    /**
     * Creates the work list view loader.
     *
     * @return the work list view loader
     */
    @Bean
    public WorkListViewLoader createWorkListViewLoader() {
        return new WorkListViewLoader();
    }
    
    /**
     * Creates the work flow loader.
     *
     * @return the work flow loader
     */
    @Bean
    public WorkFlowLoader createWorkFlowLoader() {
        return new WorkFlowLoader();
    }
    
    /**
     * Creates the view loader.
     *
     * @return the view loader
     */
    @Bean
    public ViewLoader createViewLoader() {
        return new ViewLoader();
    }
    
    /**
     * Creates the acord to work item loader.
     *
     * @return the acord to work item loader
     */
    @Bean
    public AcordToWorkItemLoader createAcordToWorkItemLoader() {
        return new AcordToWorkItemLoader();
    }
    
    /**
     * Creates the option list loader.
     *
     * @return the option list loader
     */
    @Bean
    public OptionListLoader createOptionListLoader() {
        return new OptionListLoader();
    }
    
    /**
     * Creates the dynamic template list loader.
     *
     * @return the dynamic template list loader
     */
    @Bean
    public DynamicTemplateListLoader createDynamicTemplateListLoader() {
        return new DynamicTemplateListLoader();
    }
    
    /**
     * Creates the work comp class code loader.
     *
     * @return the work comp class code loader
     */
    @Bean
    public WorkCompClassCodeLoader createWorkCompClassCodeLoader() {
        return new WorkCompClassCodeLoader();
    }
    
    /**
     * Creates the industry code loader.
     *
     * @return the industry code loader
     */
    @Bean
    public IndustryCodeLoader createIndustryCodeLoader() {
        return new IndustryCodeLoader();
    }
    
    /**
     * Creates the xarc rules loader.
     *
     * @return the xarc rules loader
     */
    @Bean
    public XarcRulesLoader createXarcRulesLoader() {
        return new XarcRulesLoader();
    }
    
    /**
     * Creates the menu loader.
     *
     * @return the menu loader
     */
    @Bean
    public MenuLoader createMenuLoader() {
        return new MenuLoader();
    }
    
    /**
     * Creates the transformer loader.
     *
     * @return the transformer loader
     */
    @Bean
    public TransformerLoader createTransformerLoader() {
        return new TransformerLoader();
    }
    
    /**
     * Creates the work item assistant loader.
     *
     * @return the work item assistant loader
     */
    @Bean
    public WorkItemAssistantLoader createWorkItemAssistantLoader() {
        return new WorkItemAssistantLoader();
    }
    
    /**
     * Creates the batch loader.
     *
     * @return the batch loader
     */
    @Bean
    public BatchLoader createBatchLoader() {
        return new BatchLoader();
    }
    /**
     * Creates the PDFDefinition loader.
     *
     * @return the PDFDefinition Loader
     */
    @Bean
    public PdfdefinitionsLoader createPDFDefinitionLoader() {
        return new PdfdefinitionsLoader();
    }

}
