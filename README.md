# README #

### Prerequisites ###
* jdk 8
* eclipse
* maven
* tomcat
* mysql

### Database ###

Loading Configuration:
  Auto loaded during startup
 
Provide a JNDI datasource to tomcat named 'configuration'. 

### Configuration ###
Local properties files can be specified with username prefix -  {username}.framework.properties/{username}.test.properties
example: adoss.framework.properties

### Testing ###
Run mvn package to run all tests
Run mvn sonar:sonar to run sonar.

### JPA entity Naming Convention ###
Table name and Column name uses underscore .

### Liquibase ###